#!/bin/bash
set -euo pipefail
set -x

#
# Run this script in this repo to pull local config file updates into the repo
#

cp -u $HOME/.bashrc \
    $HOME/.dialogrc \
    $HOME/.gitconfig \
    $HOME/.tmux.conf \
    .

mkdir -p ./.config/fish/
cp -u $HOME/.config/fish/config.fish ./.config/fish/
cp -Ru $HOME/.config/nvim ./.config/
cp -Ru $HOME/.config/kitty ./.config/

