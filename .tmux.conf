#########################################
# Colors
#########################################

# color
set -g default-terminal "screen-256color"

# https://github.com/seebi/tmux-colors-solarized/blob/master/tmuxcolors-256.conf
set -g status-style "bg=colour235, fg=colour136"
#set -g status-style "attr=default"

# default window title colors
set -g window-status-style "fg=colour244, bg=default"
#set-window-option -g window-status-attr dim

# active window title colors
set -g window-status-current-style "fg=colour166, bg=default"
#set-window-option -g window-status-current-attr bright

# pane border
set -g pane-border-style "fg=colour235"
set -g pane-active-border-style "fg=colour240"

# message text
set -g message-style "bg=colour235, fg=colour166"

# pane number display
set-option -g display-panes-active-colour colour33 #blue
set-option -g display-panes-colour colour166 #orange

# clock
set-window-option -g clock-mode-colour green #green

#########################################
# General Settings
#########################################

# Start numbering at 1
set -g base-index 1

# Allows for faster key repetition
set -s escape-time 0

# auto window rename
set-window-option -g automatic-rename on
set-option -g allow-rename on

# Activity monitoring
setw -g monitor-activity on
set -g visual-activity on

############################################################################
# Unbindings
############################################################################
unbind [   # copy mode bound to escape key
unbind j
unbind C-b # unbind default leader key
unbind '"' # unbind horizontal split
unbind %   # unbind vertical split

#########################################
# Key Binds
#########################################

# C-b is not acceptable -- Vim uses it
set-option -g prefix C-a

# Allows us to use C-a a <command> to send commands to a TMUX session inside
# another TMUX session
bind-key a send-prefix

bind-key Enter copy-mode

# Vi copypaste mode
set-window-option -g mode-keys vi
unbind p
bind p paste-buffer
bind-key -T copy-mode-vi 'v' send -X begin-selection
bind-key -T copy-mode-vi 'V' send -X select-line
bind-key -T copy-mode-vi 'y' send -X copy-selection-and-cancel

# Set window split
bind-key | split-window -h
bind-key - split-window

# Window traversal
bind-key C-a last-window
bind C-h previous-window
bind C-l next-window

# Pane traversal
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

