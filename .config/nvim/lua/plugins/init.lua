return {
    -- Colors
    "itchyny/lightline.vim",
    {
        "dikiaap/minimalist",
        lazy = false,
        config = function()
          vim.cmd("colorscheme minimalist")
        end,
    },
    -- Fuzzy finder
    {
        "ibhagwan/fzf-lua",
        config = function()
            require('fzf-lua').setup({
                lines = {
                    --previewer         = "builtin",    -- set to 'false' to disable
                    --prompt            = 'Lines❯ ',
                    show_unloaded     = true,         -- show unloaded buffers
                    show_unlisted     = false,        -- exclude 'help' buffers
                    no_term_buffers   = true,         -- exclude 'term' buffers
                    fzf_opts = {
                      -- do not include filename in lines matches
                      -- tiebreak by line no.
                      ["--delimiter"] = "[:]",
                      ["--with-nth"]  = '2..',
                      ["--nth"]       = '1..',
                      ["--tiebreak"]  = 'index',
                      ["--tabstop"]   = "1",
                    }
                },
                blines = {
                    --previewer         = "builtin",    -- set to 'false' to disable
                    --prompt            = 'Lines❯ ',
                    show_unloaded     = true,         -- show unloaded buffers
                    show_unlisted     = false,        -- exclude 'help' buffers
                    no_term_buffers   = true,         -- exclude 'term' buffers
                    fzf_opts = {
                      -- do not include filename in lines matches
                      -- tiebreak by line no.
                      ["--delimiter"] = "[:]",
                      ["--with-nth"]  = '2..',
                      ["--nth"]       = '1..',
                      ["--tiebreak"]  = 'index',
                      ["--tabstop"]   = "1",
                    }
                }
            })
        end,
    },
    -- Git
    "tpope/vim-fugitive",
    "airblade/vim-gitgutter",
    "mhinz/vim-signify",
    {
        "NeogitOrg/neogit",
        dependencies = {
            "nvim-lua/plenary.nvim",         -- required
            "sindrets/diffview.nvim",        -- optional
            -- Only one of these is needed, not both.
            --"nvim-telescope/telescope.nvim", -- optional
            --"ibhagwan/fzf-lua",              -- optional
        },
        config = true
    },
    -- Sessions
    {
      'rmagatti/auto-session',
      lazy = false,
      -- dependencies = {
      --   'nvim-telescope/telescope.nvim', -- Only needed if you want to use session lens
      -- },

      ---enables autocomplete for opts
      ---@module "auto-session"
      ---@type AutoSession.Config
      opts = {
        suppressed_dirs = {
            '~/',
            '~/Desktop',
            '~/Documents',
            '~/Downloads',
            '~/Music',
            '~/Pictures',
            '~/Video',
            '/'
        },
        -- log_level = 'debug',
      }
    },
    -- File explorer
    {
        'nvim-tree/nvim-tree.lua',
        config = function()
            require("nvim-tree").setup()
        end,
    },
    -- Terminals
    'voldikss/vim-floaterm',
    -- File syntax
    "ekalinin/Dockerfile.vim",
    "jvirtanen/vim-hcl",
    "vim-crystal/vim-crystal",
    "ziglang/zig.vim"
}
