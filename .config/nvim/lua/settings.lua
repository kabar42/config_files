-- General
vim.opt.number = true
vim.opt.list = false
vim.opt.wrap = true
vim.opt.lazyredraw = true
vim.opt.cursorline = false
vim.opt.laststatus = 2
-- Tabs
vim.opt.expandtab = true
vim.opt.smarttab = true
vim.opt.shiftwidth = 4
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
-- File backup
vim.opt.backup = false
vim.opt.wb = false
vim.opt.swapfile = false
-- Search
vim.opt.ignorecase = true
vim.opt.hlsearch = true
vim.opt.incsearch = true
-- Mouse
vim.opt.mouse = ""
-- File Explorer
-- disable netrw at the very start of your init.lua
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
--vim.g.netrw_banner = 0          -- Hide the banner
--vim.g.netrw_liststyle = 3       -- Tree style view
-- Color terminal performance
vim.cmd("hi NonText cterm=NONE ctermfg=NONE")
vim.opt.scrolloff = 1
vim.opt.scrolljump = 2
vim.opt.ttyfast = true
-- Colors
vim.opt.background = dark
vim.opt.termguicolors = true
-- Grep
vim.opt.grepprg = "rg --vimgrep --no-heading --smart-case"

-- Keybinds
vim.g.mapleader = ' '
--   Check for plugin updates
vim.keymap.set('n', '<Leader>p', ':Lazy check<CR>')
--   Vim
vim.keymap.set('n', '<Leader>e', ':NvimTreeFocus<CR>')
--vim.keymap.set('n', '<Leader>E', ':Lexplor<CR>')
--   FZF search commands
vim.keymap.set('n', '<C-p>',     ':FzfLua files<CR>')               -- Search file names
vim.keymap.set('n', '<Leader>b', ':FzfLua buffers<CR>')             -- Search buffer names
vim.keymap.set('n', '<Leader>/', ':FzfLua lines<CR>')               -- Search lines in open buffers
vim.keymap.set('n', '<Leader>:', ':FzfLua command_history<CR>')     -- Search vim command history
vim.keymap.set('n', '<Leader>g', ':FzfLua live_grep<CR>')           -- Grep all project files
vim.keymap.set('n', '<Leader>k', ':FzfLua grep_cword<CR>')          -- Grep word under cursor
vim.keymap.set('n', '<Leader>K', ':FzfLua grep_cWORD<CR>')          -- Grep WORD under cursor
vim.keymap.set('v', '<Leader>k', ':FzfLua grep_visual<CR>')         -- Grep visual selection
--vim.keymap.set('n', '<Leader>c', ':FzfLua tags<CR>')                -- Search tags
--vim.keymap.set('n', '<Leader>C', ':FzfLua tags_grep<CR>')           -- Grep tags
--vim.keymap.set('n', '<Leader>t', ':FzfLua tags_grep_cword<CR>')     -- Tags grep word under cursor
--vim.keymap.set('n', '<Leader>T', ':FzfLua tags_grep_cWORD<CR>')     -- Tags grep WORD under cursor
--vim.keymap.set('v', '<Leader>t', ':FzfLua tags_grep_visual<CR>')    -- Tags grep visual selection
vim.keymap.set('n', '<Leader>r', ':FzfLua resume<CR>')              -- Resume last search
--   Git integration
vim.keymap.set('n', '<Leader>a', ':Git blame<CR>')
vim.keymap.set('n', '<Leader>s', ':Neogit kind=split_above<CR>')
vim.keymap.set('n', '<Leader>d', ':Git diff<CR>')
vim.keymap.set('n', '<Leader>l', ':FloatermNew --name=git --width=0.95 --height=0.95 lazygit<CR>')
--   Terminal
vim.keymap.set('n', '<Leader>f', ':FloatermNew --width=0.9 --height=0.9<CR>')
vim.keymap.set('n', '<F9>', ':FloatermToggle<CR>')
vim.keymap.set('t', '<F9>', '<C-\\><C-n>:FloatermToggle<CR>')
vim.keymap.set('t', '<F10>', '<C-\\><C-n>:FloatermPrev<CR>')
vim.keymap.set('t', '<F11>', '<C-\\><C-n>:FloatermNext<CR>')

-- Autocmds
--   Remove trailing spaces on all saved files
vim.api.nvim_create_autocmd({"BufWritePre"}, {
    pattern = {'*'},
    command = [[:%s/\s\+$//e]],
})
-- Syntax for salt files set to yaml
vim.api.nvim_create_autocmd({"BufRead","BufNew"}, {
    pattern = {'*.sls'},
    command = [[:set ft=yaml]],
})

