if status is-interactive
    # Commands to run in interactive sessions can go here
    command -v brew || export PATH="$PATH:/opt/homebrew/bin:/home/linuxbrew/.linuxbrew/bin:/usr/local/bin"
    command -v brew && eval "$(brew shellenv)"
end
