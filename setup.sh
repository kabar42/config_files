#!/bin/bash
set -euo pipefail
set -x

debs=(curl
dialog
fonts-hack-ttf
libnotify-bin
python3-pip
wget)

brewapps=(axel
bat
btop
entr
eza
fd
fish
fzf
git
git-delta
iftop
jless
jq
lazydocker
lazygit
moar
neovim
procs
ripgrep
shellcheck
tig
tmux
xh
yazi
yq)

# Apt
[[ $(command -v apt) ]] && sudo apt update && sudo apt install -y ${debs[@]}

# Homebrew
[[ ! $(command -v brew) ]] && export HOMEBREW_NO_INSTALL_FROM_API=1 ; export NONINTERACTIVE=1 ; /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" && \
    eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

brew install -q ${brewapps[@]}

# Git config
# - Runs after the brew install for git
git config --global pull.rebase false

# Copy config files
cp -u .bashrc $HOME/
cp -u .dialogrc $HOME/
cp -u .gitconfig $HOME/
cp -u .tmux.conf $HOME/

# Neovim config
mkdir -p $HOME/.config/nvim/
cp -Ru ./.config/nvim $HOME/.config/

# kitty config
mkdir -p $HOME/.config/kitty/
cp -Ru ./.config/kitty $HOME/.config/

# Kitty terminal
if [[ ! $(command -v kitty) ]] ; then
    mkdir -p ${HOME}/.local/bin/
    mkdir -p ${HOME}/.local/share/applications/
    # Install the binaries
    curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin launch=n
    # Create symbolic links to add kitty and kitten to PATH (assuming ~/.local/bin is in
    # your system-wide PATH)
    ln -sf ~/.local/kitty.app/bin/kitty ~/.local/kitty.app/bin/kitten ~/.local/bin/
    # Place the kitty.desktop file somewhere it can be found by the OS
    cp ~/.local/kitty.app/share/applications/kitty.desktop ~/.local/share/applications/
    # If you want to open text files and images in kitty via your file manager also add the kitty-open.desktop file
    cp ~/.local/kitty.app/share/applications/kitty-open.desktop ~/.local/share/applications/
    # Update the paths to the kitty and its icon in the kitty desktop file(s)
    sed -i "s|Icon=kitty|Icon=$(readlink -f ~)/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png|g" ~/.local/share/applications/kitty*.desktop
    sed -i "s|Exec=kitty|Exec=$(readlink -f ~)/.local/kitty.app/bin/kitty|g" ~/.local/share/applications/kitty*.desktop
    # Make xdg-terminal-exec (and hence desktop environments that support it use kitty)
    echo 'kitty.desktop' > ~/.config/xdg-terminals.list
fi

