set nocompatible
set backspace=eol,start,indent

"=== Plugins
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
"=== Colors
Plugin 'dikiaap/minimalist'
"=== Editor
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'itchyny/lightline.vim'
"=== SCM
Plugin 'tpope/vim-fugitive'
Plugin 'airblade/vim-gitgutter'
Plugin 'mhinz/vim-signify'
"=== Syntax
Plugin 'python-mode/python-mode'
Plugin 'ekalinin/Dockerfile.vim'
Plugin 'chr4/nginx.vim'
Plugin 'lepture/vim-jinja'
"Plugin 'crazy-canux/icinga2.vim'
"Plugin 'zah/nim.vim'
"Plugin 'fatih/vim-go'
"Plugin 'rust-lang/rust.vim'
Plugin 'gleam-lang/gleam.vim'
"Plugin 'elixir-editors/vim-elixir'
"Plugin 'elixir-lang/vim-elixir'
"Plugin 'dleonard0/pony-vim-syntax'
"Plugin 'udalov/kotlin-vim'
call vundle#end()
filetype plugin indent on

"=== General
syntax enable
set number          " Turn on line numbers
set wrap            " Wrap lines
set lazyredraw      " Lazy redraw when executing macros
set nocursorline
set laststatus=2    " Always show the status line

"=== Tabs
set expandtab       " Use spaces instead of tabs
set smarttab
set shiftwidth=4
set tabstop=4
set softtabstop=4

"=== Indent
set ai              " Auto indent
set si              " Smart indent

"=== Search
set incsearch
set hlsearch
set ignorecase

"=== File Backup
set nobackup
set nowb
set noswapfile

"=== Performance improvements for color terminals
hi NonText cterm=NONE ctermfg=NONE
set scrolloff=1
set scrolljump=2
set ttyscroll=2
set ttyfast

"=== Colors
set background=dark
set term=screen-256color
set t_Co=256
colorscheme minimalist

"=== Filetypes
au BufNewFile,BufRead Dockerfile        setfiletype dockerfile
"au BufNewFile,BufRead */*nginx*/*.conf  setfiletype nginx
"au BufNewFile,BufRead *.sls             setfiletype yaml
"au BufNewFile,BufRead *.jinja           setfiletype jinja
"au BufNewFile,BufRead */icinga2/*.conf  setfiletype icinga2

"=== Key binds
" Tab navigation
nmap <C-t>c :tabnew<cr>
nmap <C-t>x :tabclose<cr>
nmap <C-t>k :tabr<cr>
nmap <C-t>j :tabl<cr>
nmap <C-t>h :tabp<cr>
nmap <C-t>l :tabn<cr>

"=== Commands
" Remove trailing whitespace
autocmd BufWritePre * :%s/\s\+$//e
" Automatically reload config changes
"autocmd BufWritePost .vimrc source $MYVIMRC

"=== Search tools
if executable('ag')
  set grepprg=ag\ --nogroup\ --nocolor
  "let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
  "let g:ctrlp_use_caching = 0
endif

" Search visually selected text
vnoremap // y\<C-R>"<CR>
" Search word under cursor with K
nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>

command! Vundle !git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

" Bind <leader>s to grep command
command! -nargs=+ -complete=file -bar Ag silent! grep! <args>|cwindow|redraw!
nnoremap <leader>s :Ag<SPACE>
